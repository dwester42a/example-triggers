package com.example;


import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.plugin.triggers.api.*;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Created by daniel on 9/16/16.
 */
public class ChangeSummaryTrigger implements WorkflowTrigger
{

    private final IssueManager issueManager;

    private final UserManager userManager;
    public ChangeSummaryTrigger( final IssueManager issueManager,
                                 final UserManager userManager)
    {
        this.issueManager = issueManager;
        this.userManager = userManager;
    }

    public String describe(Map<String, String> map)
    {
        return "Description text from com.example.ChangeSummaryTrigger";
    }

    @Nonnull
    public HistoryMetadata createTransitionMetadata(@Nonnull WorkflowEvent workflowEvent)
    {
        UpdateIssueRequest updateIssueRequest = UpdateIssueRequest.builder().eventDispatchOption(EventDispatchOption.DO_NOT_DISPATCH).sendMail(false).build();
        ApplicationUser theUser = this.userManager.getUserByName( workflowEvent.getInitiator().getName() );
        for(String issueKey: workflowEvent.getIssueKeys())
        {

            MutableIssue issue = this.issueManager.getIssueByKeyIgnoreCase( issueKey);
            issue.setSummary("Some new summary");
            this.issueManager.updateIssue(theUser, issue, updateIssueRequest);
        }
        return null;
    }

    public boolean isAvailable()
    {
        return true;
    }

    public boolean shouldExecute(@Nonnull WorkflowEvent workflowEvent, @Nonnull Issue issue, @Nonnull Map<String, String> map) {

        return true;
    }

    public void onSuccessfulTransition(@Nonnull TriggerExecutionContext triggerExecutionContext)
    {
        System.err.println("onSuccessfulTransition");

    }
}

