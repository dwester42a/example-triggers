package com.example;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.triggers.api.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by daniel on 9/16/16.
 */
public class RemoteTriggerEvent implements RemoteWorkflowEvent, WorkflowEvent
{
    private RemoteInitiator initiator;
    private String issueKey;
    private User localUser;

    public RemoteTriggerEvent(RemoteInitiator initiator,User localUser, String issueKey) {
        this.initiator = initiator;
        this.issueKey = issueKey;
        this.localUser = localUser;
    }

    @Nullable
    public RemoteInitiator getRemoteInitiator()
    {

        return this.initiator;
    }

    @Nonnull
    public EventSource getSource()
    {
        return new EventSource.Builder("Somewhere", "a type", "http://example.com", "http://example.com/avatar").build();
    }

    @Nonnull
    public Set<String> getIssueKeys()
    {
        Set<String> issueKeys = new HashSet<String>();
        issueKeys.add( this.issueKey);
        return issueKeys;
    }

    @Nullable
    public User getInitiator()
    {
        return this.localUser;
    }
}
