package com.example;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugin.triggers.api.RemoteInitiator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by daniel on 9/16/16.
 */
@Path("/example-trigger")
public class RestTriggerEndPoint
{

    private final UserManager userManager;
    private final EventPublisher eventPublisher;

    private final static String userName = "admin";

    public RestTriggerEndPoint( final UserManager userManager,
                                final EventPublisher eventPublisher)
    {
        this.userManager = userManager;
        this.eventPublisher = eventPublisher;
    }

    @Path("/{issue}")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response triggerEvent(@PathParam("issue") String issue)
    {
        ApplicationUser applicationUser = this.userManager.getUserByName( this.userName);

        RemoteInitiator remoteInitiator = new RemoteInitiator.Builder(applicationUser.getUsername()).addEmail(applicationUser.getEmailAddress()).displayName(applicationUser.getDisplayName()).build();

        RemoteTriggerEvent remoteTriggerEvent = new RemoteTriggerEvent(remoteInitiator, applicationUser.getDirectoryUser(), issue);


        eventPublisher.publish( remoteTriggerEvent);
        return Response.ok().build();
    }
}
